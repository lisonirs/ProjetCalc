package application;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			Parent rootFXML = FXMLLoader.load(getClass().getResource("VueCalc.fxml"));
			Scene sceneFXML = new Scene(rootFXML);
			
			//Title de mon application
			primaryStage.setTitle("Calculator Java FX");
			
			//Attribut la composition de ma fen�tre � ma fen�tre principale
			primaryStage.setScene(sceneFXML);
			
			//Affiche la fen�tre principale
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
