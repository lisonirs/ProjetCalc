package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class ControllerCalc {
	
	private String premierNum;
	private String operateur;
	
	
	@FXML
	private Button btnMutiplier;
	
	@FXML
	private Button btnAdditionner;
	
	@FXML
	private Button btnSoustraire;
	
	@FXML
	private Button btnDiviser;
	
	
	@FXML
	private Button btnEgal;
	
	@FXML
	private Button btnPoint;
	
	
	@FXML
	private Button btnZero;
	
	@FXML
	private Button btnUn;
	
	@FXML
	private Button btnDeux;
	
	@FXML
	private Button btnTrois;
	
	@FXML
	private Button btnQuatre;
	
	@FXML
	private Button btnCinq;

	@FXML
	private Button btnSix;
	
	@FXML
	private Button btnSept;
	
	@FXML
	private Button btnHuit;
	
	@FXML
	private Button btnNeuf;
	
	@FXML
	private Label lblResultat;
	
	public void Resultat(ActionEvent e) {
		String resultat = e.getSource().toString();
		//System.out.print(resultat);
		resultat = resultat.substring(resultat.indexOf("'")+1, resultat.lastIndexOf("'"));
		
		
		switch (resultat){
			case "+":
				operateur = "+";
				premierNum = lblResultat.getText();
				lblResultat.setText("");
				break;
			case "-":
				operateur = "-";
				premierNum = lblResultat.getText();
				lblResultat.setText("");
				break;
			case "/":
				operateur = "/";
				premierNum = lblResultat.getText();
				lblResultat.setText("");
				break;
			case "x":
				operateur = "x";
				premierNum = lblResultat.getText();
				lblResultat.setText("");
				break;
				
			case "=":
				
				switch (operateur) {
				case "+":
		//			lblResultat.setText(Integer.parseInt(premierNum) + Integer.parseInt(lblResultat.getText()));
					break;
				case "-":
					lblResultat.setText(premierNum + lblResultat.getText());
					break;
				}
			
				
				break;	
			default :
				lblResultat.setText(lblResultat.getText() + resultat);
			;
		}
	}
}
